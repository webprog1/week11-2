import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class productsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}
  create(createproductDto: CreateProductDto) {
    const product: Product = new Product();
    product.name = createproductDto.name;
    product.price = createproductDto.price;
    return this.productsRepository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.productsRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.productsRepository.findOneBy({ id: id });
  }

  async update(id: number, updateproductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneBy({ id: id });
    const updatedproduct = { ...product, ...updateproductDto };
    return this.productsRepository.save(updatedproduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOneBy({ id: id });
    return this.productsRepository.remove(product);
  }
}
