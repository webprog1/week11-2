import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  price: number;
}
